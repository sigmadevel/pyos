from setuptools import setup, find_packages

setup (name = 'pyos',
        package_dir = {'pyos' : 'pyos'},
        packages = find_packages()
        )
