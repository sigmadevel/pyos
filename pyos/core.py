﻿import types
from pyos.utils import Background_Call
from collections import deque

class Task(object):
    taskid = 0
    def __init__(self,target):
        Task.taskid += 1
        self.tid     = Task.taskid   # Task ID
        self.target  = target        # Target coroutine
        self.sendval = None          # Value to send
        self.stack   = []            # Call stack

    # Run a task until it hits the next yield statement
    def run(self):
        while True:
            try:
                result = self.target.send(self.sendval)
                if isinstance(result,SystemCall): return result
                if isinstance(result,types.GeneratorType):
                    self.stack.append(self.target)
                    self.sendval = None
                    self.target  = result
                else:
                    if not self.stack: return
                    self.sendval = result
                    self.target  = self.stack.pop()
            except StopIteration:
                if not self.stack: raise
                self.sendval = None
                self.target = self.stack.pop()

import select

class Scheduler(object):
    def __init__(self):
        self.ready   = deque()
        self.taskmap = {}

        # Tasks waiting for other tasks to exit
        self.exit_waiting = {}

        # I/O waiting
        self.read_waiting = {}
        self.write_waiting = {}
        self.thread_waiting= {}
        self.new(self.thread_waiting_task())
        self.new(self.io_task())

    def new(self,target):
        newtask = Task(target)
        self.taskmap[newtask.tid] = newtask
        self.schedule(newtask)
        return newtask.tid

    def exit(self,task):
        del self.taskmap[task.tid]
        # Notify other tasks waiting for exit
        for task in self.exit_waiting.pop(task.tid,[]):
            self.schedule(task)

    def wait_for_exit(self,task,waittid):
        if waittid in self.taskmap:
            self.exit_waiting.setdefault(waittid,[]).append(task)
            return True
        else:
            return False

    # I/O waiting
    def wait_for_thread(self,task,obj):
        self.thread_waiting[obj] = task

    def wait_for_read(self,task,fd):
        self.read_waiting[fd] = task

    def wait_for_write(self,task,fd):
        self.write_waiting[fd] = task

        
    #Pollers
    def thread_waiting_poll(self):
        for obj in self.thread_waiting.keys():
            if obj.is_done():
                self.schedule(self.thread_waiting.pop(obj))


    def io_poll(self,timeout):
        if self.read_waiting or self.write_waiting:
           r,w,e = select.select(self.read_waiting,
                                 self.write_waiting,[],timeout)
           for fd in r: self.schedule(self.read_waiting.pop(fd))
           for fd in w: self.schedule(self.write_waiting.pop(fd))
    
    #poller tasks
    def thread_waiting_task(self):
        while True:
            self.thread_waiting_poll()
            yield

    def io_task(self):
        while True:
            self.io_poll(0)
            yield

    def schedule(self,task):
        self.ready.append(task)

    def run(self):
         while self.taskmap:
            self.main_pulse()

    def main_pulse(self):
        for i in range(0,len(self.ready)):
            task = self.ready.popleft()
            try:
                result = task.run()
                if isinstance(result,SystemCall):
                        result.task  = task
                        result.sched = self
                        result.handle()
                        continue
            except StopIteration:
                self.exit(task)
                continue
            self.schedule(task)

class SystemCall(object):
    def handle(self):
        pass

# Return a task's ID number
class GetTid(SystemCall):
    def handle(self):
        self.task.sendval = self.task.tid
        self.sched.schedule(self.task)

# Create a new task
class NewTask(SystemCall):
    def __init__(self,target):
        self.target = target
    def handle(self):
        tid = self.sched.new(self.target)
        self.task.sendval = tid
        self.sched.schedule(self.task)

# Kill a task
class KillTask(SystemCall):
    def __init__(self,tid):
        self.tid = tid
    def handle(self):
        task = self.sched.taskmap.get(self.tid,None)
        if task:
            task.target.close() 
            self.task.sendval = True
        else:
            self.task.sendval = False
        self.sched.schedule(self.task)

# Wait for a task to exit
class WaitTask(SystemCall):
    def __init__(self,tid):
        self.tid = tid
    def handle(self):
        result = self.sched.wait_for_exit(self.task,self.tid)
        self.task.sendval = result
        # If waiting for a non-existent task,
        # return immediately without waiting
        if not result:
            self.sched.schedule(self.task)

# Wait for reading
class ReadWait(SystemCall):
    def __init__(self,f):
        self.f = f
    def handle(self):
        fd = self.f.fileno()
        self.sched.wait_for_read(self.task,fd)

# Wait for writing
class WriteWait(SystemCall):
    def __init__(self,f):
        self.f = f
    def handle(self):
        fd = self.f.fileno()
        self.sched.wait_for_write(self.task,fd)

# Wait for Thread
class ThreadWait(SystemCall):
    def __init__(self, obj):
        self.obj = obj
    def handle(self):
        self.sched.wait_for_thread(self.task,self.obj)

# Create new task and wait for it
class NewWaitTask(SystemCall):
    def __init__(self,target):
        self.target = target
    def handle(self):
        new_task = Task(self.target)
        self.sched.taskmap[new_task.tid] = new_task
        self.task.sendval = new_task.tid
        self.sched.schedule(new_task)
        self.sched.wait_for_exit(self.task,new_task.tid)


from functools import wraps

def non_blocking(func):
    @wraps(func)
    def wrapper(*args,**kwargs):
        background_call = Background_Call(func, *args, **kwargs)
        yield ThreadWait(background_call)
        yield background_call.get_return()
    return wrapper

def new_wait_task(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        yield NewWaitTask(func(*args, **kwargs))
    return wrapper

@new_wait_task
def until(func, *args, **kwargs):
    invert = kwargs.pop('invert', False)
    predicate = kwargs.pop('predicate', lambda x:x and True)
    #logging.debug(func)
    while True:
        if isinstance(func, types.FunctionType):
            is_met = yield func(*args)
        else:
            raise TypeError(repr(func) + 'Must, be a function,\
            maybe try a lambda')
        #logging.debug(is_met)
        if invert:
            if not predicate(is_met):
                break
        else:
            if predicate(is_met):
                break


@new_wait_task
def wait(secs):
    from datetime import datetime, timedelta
    now = datetime.now()
    end = now + timedelta(0,secs,0,0,0,0,0)
    while end > datetime.now():
        yield
